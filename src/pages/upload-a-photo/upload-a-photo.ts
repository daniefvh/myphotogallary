import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the UploadAPhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upload-a-photo',
  templateUrl: 'upload-a-photo.html',
})
export class UploadAPhotoPage {

  private uploadPhotoForm : FormGroup;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder) {
    this.uploadPhotoForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
    });
  }

  uploadPhoto(){

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadAPhotoPage');
  }

}
