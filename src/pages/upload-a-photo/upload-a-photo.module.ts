import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadAPhotoPage } from './upload-a-photo';

@NgModule({
  declarations: [
    UploadAPhotoPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadAPhotoPage),
  ],
})
export class UploadAPhotoPageModule {}
